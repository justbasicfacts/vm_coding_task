import { InMemoryDbService } from 'angular-in-memory-web-api';
import { CategoryItem } from './category-item';
export class CategoriesService implements InMemoryDbService {
  createDb(): any {
    const categories: Array<CategoryItem> = [];
    return { categories };
  }
}
