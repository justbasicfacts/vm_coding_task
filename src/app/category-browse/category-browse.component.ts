import { Component } from '@angular/core';
import { DataService } from '../data-service';
import { Debounce } from 'lodash-decorators';

@Component({
  selector: 'app-category-browse',
  templateUrl: './category-browse.component.html',
  styleUrls: ['./category-browse.component.css'],
})
export class CategoryBrowseComponent {
  public searchTerm: any;
  public results: any;
  public allItems: any;
  public searching: boolean;

  constructor(private dataService: DataService) {
    this.searching = false;
  }

  @Debounce(100)
  searchItem(): void {
    this.searching = true;
    this.dataService.searchCategory(this.searchTerm).subscribe((data) => {
      this.results = data;
      this.searching = false;
    });
  }
}
