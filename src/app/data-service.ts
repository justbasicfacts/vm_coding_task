import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { throwError, Observable } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import { CategoryItem } from './category-item';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  apiurl = 'api/categories';

  headers = new HttpHeaders()
    .set('Content-Type', 'application/json')
    .set('Accept', 'application/json');
  httpOptions = {
    headers: this.headers,
  };

  constructor(private http: HttpClient) {}

  private handleError(error: any): Observable<never> {
    console.log(error);
    return throwError(error);
  }

  getCategories(): Observable<CategoryItem[]> {
    return this.http.get<CategoryItem[]>(this.apiurl).pipe(
      tap((data) => console.log(data)),
      catchError(this.handleError)
    );
  }

  addCategory(category: CategoryItem): Observable<CategoryItem> {
    return this.http
      .post<CategoryItem>(this.apiurl, category, this.httpOptions)
      .pipe(
        tap((newCategory: CategoryItem) =>
          console.log(`added category w/ id=${newCategory.id}`)
        ),
        catchError(this.handleError)
      );
  }

  async addCategories(categoryList: CategoryItem[]): Promise<void> {
    for (const item of categoryList) {
      await this.addCategory(item).subscribe();
    }
  }

  searchCategory(searchTerm: string): Observable<CategoryItem[]> {
    const searchApiUrl = `${this.apiurl}?search=${searchTerm}`;
    return this.http.get<CategoryItem[]>(searchApiUrl).pipe(
      tap((data) => console.log(data)),
      catchError(this.handleError)
    );
  }
}
