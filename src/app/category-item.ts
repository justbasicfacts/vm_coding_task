export class CategoryItem {
  id: number;
  subCategories: Array<string>;
  search: string;

  constructor(id: number, subCategories: Array<string>) {
    this.id = id;
    this.subCategories = subCategories;
    this.search = subCategories.toString();
  }
}
