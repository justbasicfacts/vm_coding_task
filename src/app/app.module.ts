import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CategoryAddComponent } from './category-add/category-add.component';
import { CategoryBrowseComponent } from './category-browse/category-browse.component';
import { CategoryListItemComponent } from './category-list-item/category-list-item.component';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { CategoriesService } from './categories-service';
@NgModule({
  declarations: [
    AppComponent,
    CategoryAddComponent,
    CategoryBrowseComponent,
    CategoryListItemComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    InMemoryWebApiModule.forRoot(CategoriesService),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
