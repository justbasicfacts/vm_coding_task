import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-category-list-item',
  templateUrl: './category-list-item.component.html',
  styleUrls: ['./category-list-item.component.css'],
})
export class CategoryListItemComponent  {
  @Input() id: any;
  @Input() subCategories: any;

  alertId(): void {
    alert('Clicked category id is ' + this.id);
  }

}
