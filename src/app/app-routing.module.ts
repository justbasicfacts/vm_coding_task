import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CategoryAddComponent } from '../app/category-add/category-add.component';
import { CategoryBrowseComponent } from '../app/category-browse/category-browse.component';
const routes: Routes = [
  { path: 'category-add', component: CategoryAddComponent },
  { path: 'category-browse', component: CategoryBrowseComponent },
  { path: '', redirectTo: 'category-add', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
