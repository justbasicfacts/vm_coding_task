import { Component } from '@angular/core';
import { CategoryParser } from '../category-parser';
import { DataService } from '../data-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-category-add',
  templateUrl: './category-add.component.html',
  styleUrls: ['./category-add.component.css'],
})
export class CategoryAddComponent {
  public category: any;
  public categoriesInput: any;
  public errorMessage: any;
  constructor(private dataService: DataService, private router: Router) {}

  parseCategories(): void {
    try {
      const categories = CategoryParser.parseInputToCategoryItem(
        this.categoriesInput
      );

      this.dataService.addCategories(categories).then(() => {
        this.router.navigate(['/category-browse']);
      });
    } catch (err) {
      this.errorMessage = err.message;
    }
  }
}
