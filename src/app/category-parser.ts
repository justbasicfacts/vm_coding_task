import { CategoryItem } from './category-item';

export class CategoryParser {
  static parseInputToCategoryItem(data: string): Array<CategoryItem> {
    if (!data || !data.trim()) {
      throw new Error('input data cannot be empty or blank');
    }

    const lines = data.split('\n');
    const categories: Array<CategoryItem> = [];

    for (const line of lines) {
      const id = line.split('-')[0];
      const idAsNumber = Number(id.trim());

      if (isNaN(idAsNumber)) {
        throw new Error('Input is not well formed');
      }

      const subCategories = line
        .substr(id.length + 2, line.length - id.length)
        .split('>')
        .map((item: string) => item.trim());

      if (subCategories.length === 0) {
        throw new Error('Input is not well formed');
      }

      categories.push(new CategoryItem(idAsNumber, subCategories));
    }
    return categories;
  }
}
