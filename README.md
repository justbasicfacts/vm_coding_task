# VmTaskApp

This is my implementation for the product search coding task of VM.

I have used angular/in-memory-web-api to mock backend api. [Angular In Memory Web API](https://github.com/angular/in-memory-web-api)

## usage of api/categories
POST -> to add new category

GET  -> to get all categories

GET (?search=<query>) -> to search within categories

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.2.

## Screenshots

[![screen1](https://i.ibb.co/xmjdmPH/screen1.png)](https://ibb.co/B4ZF4dN) [![screen2](https://i.ibb.co/6NjTDgd/screen2.png)](https://ibb.co/g6kftzn) [![screen3](https://i.ibb.co/WK343kJ/screen3.png)](https://ibb.co/12ntnrW) [![screen4](https://i.ibb.co/9sz5ms2/screen4.png)](https://ibb.co/ChNGphv) [![screen5](https://i.ibb.co/NjxWvLg/screen5.png)](https://ibb.co/Hz7GSrL) [![screen6](https://i.ibb.co/QN2m3xj/screen6.png)](https://ibb.co/0MgJRzV)

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

